function flatten(elements, depth) {
if(depth===undefined){
 depth=1;
 }
  let flattened = [];
  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index]) && depth > 0) {
     flattened = flattened.concat(flatten(elements[index],depth-1));
    } else if (elements[index] !== undefined) {
      flattened.push(elements[index]);
    }
  }
  return flattened;
}

module.exports = flatten;

