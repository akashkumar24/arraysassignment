function reduce(elements, cb, startingValue) {
  let accumulator = startingValue;
  let startIndex = 0;

  if (startingValue === undefined) {
    accumulator = elements[0];
    startIndex = 1;
  }

  for (let index = startIndex; index < elements.length; index++) {
    accumulator = cb(accumulator, elements[index],index,elements);
  }

  return accumulator;
}

module.exports = reduce;
