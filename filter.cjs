function filter(elements, cb) {
  const filteredElements = [];
  for (let index = 0; index < elements.length; index++) {
    if (cb(elements[index],index,elements)===true) {
      filteredElements.push(elements[index]);
    }
  }
  return filteredElements;
}

module.exports=filter;
