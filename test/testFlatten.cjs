const flatten = require('../flatten.cjs');


const arr = [1, , ,[2, 3], [4, [5, 6]]];
const flattened1 = flatten(arr,2);
const flattened2 = arr.flat(2);
console.log(flattened1);
console.log(flattened2);
console.log(flattened1.toString() === flattened2.toString());

